/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.srv;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author dcdjuanes
 */
@Service
public class ManejadorArchivos {
    
    String dirImagen = "img"; 
    String dirAudio = "audio"; 
    //Se obtiene el separador segun el OS            
    String separador = System.getProperty("file.separator");
    /*static*/ ZipOutputStream zos;
    private static final Logger logger = Logger.getLogger(ManejadorArchivos.class.getName());   
    
    /**
     * *
     * Verifica si existe el directorio principal y el del grupo, si no lo hay
     * lo crea.
     *
     * @param directorio  nombre del grupo     
     **
     */
    public void verificarDirectorio(String directorio){             
        File folderPrincipal = new File(dirImagen);
        File folder = new File(dirImagen+separador+directorio);
        
        if (!folderPrincipal.exists()) { // Creacion de directorio si no existe
            System.out.println("Directorio principal creado correctamente");
            folderPrincipal.mkdir();
        }
        if (!folder.exists()) { // Creacion de directorio si no existe
            System.out.println("Directorio creado correctamente");
            folder.mkdir();
        }
           
    }
    
    /**
     * *
     * Usa la funcion verificarDirectorio para verificar el principal y el de grupo
     * Se genera el path del archivo, y se crea/modifica 
     *
     * @param idGrupo id del grupo
     * @param idUsuario id del usuario
     * @param fileArray  array de bytes del archivo
     * @param nombreArchivo  nombre del archivo a crear/modificar     
     **
     */
    public void creacionActualizacionArchivo(Long idGrupo,Long idUsuario, byte[] fileArray,String nombreArchivo) throws IOException{
        //Se verifica si existe el directorio de grupo, en el caso de no existir se crea.        
        FileOutputStream fileOuputStream = new FileOutputStream("");
        verificarDirectorio(String.valueOf(idGrupo));        
        try{            
            String path = dirImagen+separador+String.valueOf(idGrupo)+separador+nombreArchivo;
            // Con este código se agregan los bytes al archivo y se lo guarda en la carpeta de grupo.            
            fileOuputStream = new FileOutputStream(path);
            fileOuputStream.write(fileArray);
            fileOuputStream.close();

        } catch (Exception e) {                
                logger.log(Level.INFO,"Error al leer y crear archivo");
        }finally{
            fileOuputStream.close();
        }
        
    }
    
    /**
     * *
     * Comprueba si el fichero existe.
     *
     * @param nombre nombre del fichero
     * @param idGrupo id del grupo
     * @return boolean, si existe o no el fichero
     **
     */
    public boolean comprbarExisteFicheroImagen(String nombre) {
        String path = dirImagen+separador+nombre;
        File fichero = new File(path).getAbsoluteFile();
        if (fichero.exists()) {
            System.out.println("El fichero " + path + " existe");
            return true;
        } else {
            System.out.println("El fichero " + path + " NO existe");
            return false;
        }
    }
    
    public boolean comprbarExisteFicheroAudio(String nombre) {
        String path = dirAudio+separador+nombre;
        File fichero = new File(path).getAbsoluteFile();
        if (fichero.exists()) {
            System.out.println("El fichero " + path + " existe");
            return true;
        } else {
            System.out.println("El fichero " + path + " NO existe");
            return false;
        }
    }
    
    public byte[] descargaImagen(String filename) throws IOException{
        String dataDirectory = dirImagen + separador;
        Path path = Paths.get(dataDirectory + filename);
        byte[] data = Files.readAllBytes(path);
        return data;
    }
    
    public byte[] descargaAudio(String filename) throws IOException{
        String dataDirectory = dirAudio + separador;
        Path path = Paths.get(dataDirectory + filename);
        byte[] data = Files.readAllBytes(path);
        return data;
    }
}
