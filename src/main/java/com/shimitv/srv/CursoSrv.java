/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.srv;

import com.shimitv.dao.CursoDAO;
import com.shimitv.dao.ExamenDAO;
import com.shimitv.dao.LeccionDAO;
import com.shimitv.dao.PreguntaDAO;
import com.shimitv.dao.UsuarioDAO;
import com.shimitv.dom.Curso;
import com.shimitv.dom.Examen;
import com.shimitv.dom.Leccion;
import com.shimitv.dom.LeccionCompletada;
import com.shimitv.dom.Nivel;
import com.shimitv.dom.Pregunta;
import com.shimitv.dom.RespuestaJSON;
import com.shimitv.dom.Usuario;
import com.shimitv.dom.sorter.PreguntaSorter;
import com.shimitv.wsrv.UsuarioRest;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author helio
 */
@Service
public class CursoSrv {
    private static final Logger logger = Logger.getLogger(CursoSrv.class.getName());
    @Autowired
    CursoDAO cursoDao;
    @Autowired
    LeccionDAO leccionDao;
    @Autowired
    ExamenDAO examenDao;
    @Autowired
    UsuarioSrv usuarioSrv;
    @Autowired
    PreguntaDAO preguntaDao;
    @Autowired
    UsuarioDAO usuarioDao;
    
    
    public RespuestaJSON existeIdCurso(Long idCurso) throws SQLException{   
        RespuestaJSON respuesta = new RespuestaJSON(); 
        boolean exists = cursoDao.hasCurso(idCurso);
        
        if(exists == false){
            respuesta.setEstado(false);
            respuesta.setMensaje("No es un curso registrado en el sistema");
            respuesta.setRespuesta(false);
            return respuesta;
        }
        return null;
    }
    
    public RespuestaJSON existeIdNivel(Long idNivel) throws SQLException{   
        RespuestaJSON respuesta = new RespuestaJSON(); 
        boolean exists = cursoDao.hasNivel(idNivel);
        
        if(exists == false){
            respuesta.setEstado(false);
            respuesta.setMensaje("No es un nivel registrado en el sistema");
            respuesta.setRespuesta(false);
            return respuesta;
        }
        return null;
    }
    
    public RespuestaJSON existeIdLeccion(Long idLeccion) throws SQLException{   
        RespuestaJSON respuesta = new RespuestaJSON(); 
        boolean exists = leccionDao.isLeccion(idLeccion);
        
        if(exists == false){
            respuesta.setEstado(false);
            respuesta.setMensaje("No es una lección registrada en el sistema");
            respuesta.setRespuesta(false);
            return respuesta;
        }
        return null;
    }
    
    public RespuestaJSON lecciones_nivel(Long idUsuario, Long idNivel) throws SQLException{
        RespuestaJSON respuesta = this.existeIdNivel(idNivel);
        if (respuesta != null) {
            return respuesta;
        }
        respuesta = new RespuestaJSON();
        List<Leccion> lecciones = leccionDao.get_lecciones(idNivel);
        //setear lecciones completadas
        List<LeccionCompletada> leccionesC = new ArrayList<LeccionCompletada>();
        List<Long> idleccion = new ArrayList<Long>();
        leccionesC= leccionDao.getLeccionCompletada(idUsuario, idNivel);
        for(LeccionCompletada lec: leccionesC ){
            Long idLec=lec.getIdLeccion();
            for(int i=0;i<lecciones.size();i++){
                Leccion l=lecciones.get(i);
                if (idLec==l.getIdLeccion()){
                    l.setTerminado(true);
                    lecciones.set(i, l);
                }
            }
            
        }
        Leccion l1,l2;
        l1=lecciones.get(leccionesC.size());
        
      //  l2=lecciones.get(leccionesC.size()+1);
        l1.setTerminado(true);
        lecciones.set(leccionesC.size(),l1);
        //lecciones.set(leccionesC.size()+1,l2);
                
        respuesta.setEstado(true);
        respuesta.setMensaje("Éxito");
        respuesta.setRespuesta(lecciones);
        return respuesta;
    }
    
    public RespuestaJSON nivel_curso(Long idCurso) throws SQLException{
        RespuestaJSON respuesta = this.existeIdCurso(idCurso);
        if (respuesta != null) {
            return respuesta;
        }
        respuesta = new RespuestaJSON();
        List<Nivel> lecciones = leccionDao.get_niveles_curso(idCurso);
        respuesta.setEstado(true);
        respuesta.setMensaje("Éxito");
        respuesta.setRespuesta(lecciones);
        return respuesta;
    }
    
    public RespuestaJSON get_examen_resultados(Long idNivel, Long idUsuario) throws SQLException{
        RespuestaJSON respuesta = this.existeIdNivel(idNivel);
        if (respuesta != null) {
            return respuesta;
        }
        respuesta = usuarioSrv.existeIdUsuario(idUsuario);
        if (respuesta != null) {
            return respuesta;
        }
        respuesta = new RespuestaJSON();
        List<Examen> examenes = examenDao.getExamenes(idUsuario, idNivel);
        respuesta.setEstado(true);
        respuesta.setMensaje("Éxito");
        respuesta.setRespuesta(examenes);
        return respuesta;
    }
    
    public RespuestaJSON get_cuestionario_leccion(Long idLeccion) throws SQLException{
        RespuestaJSON respuesta = this.existeIdLeccion(idLeccion);
        if (respuesta != null) {
            return respuesta;
        }
        respuesta = new RespuestaJSON();
        List<Pregunta> preguntas = preguntaDao.getCuestionario(idLeccion);
        logger.log(Level.SEVERE, "NumPreguntas: " + String.valueOf(preguntas.size()));
        
        if(preguntas.size() <= 0){
            respuesta.setEstado(false);
            respuesta.setMensaje("No existen preguntas para la lección solicitada");
            respuesta.setRespuesta(false);
        }else{
            Collections.sort(preguntas, new PreguntaSorter());
            Pregunta[] preguntaArr = new Pregunta[preguntas.size()];
            preguntaArr = preguntas.toArray(preguntaArr);
            
            respuesta.setEstado(true);
            respuesta.setMensaje("Éxito");
            respuesta.setRespuesta(preguntaArr);
        }
        
        return respuesta;
    }
    
    public RespuestaJSON save_examen(Long idUsuario, Long idNivel, Long idCurso, String calificacion) throws SQLException {
        RespuestaJSON respuesta = usuarioSrv.existeIdUsuario(idUsuario);
        if (respuesta != null) {
            return respuesta;
        }
        respuesta = this.existeIdNivel(idNivel);
        if (respuesta != null) {
            return respuesta;
        }
        respuesta = this.existeIdCurso(idCurso);
        if (respuesta != null) {
            return respuesta;
        }
        respuesta = new RespuestaJSON();
        Usuario u = usuarioDao.getUserById(idUsuario);
        Curso c = cursoDao.getCurso(idCurso);
        Nivel n = cursoDao.getNivel(idNivel);
        Examen e = new Examen(u, c, n, calificacion);
        examenDao.saveExamen(e);
        respuesta.setEstado(true);
        respuesta.setMensaje("Examen almacenado");
        respuesta.setRespuesta(true);
        return respuesta;
    }

    public RespuestaJSON get_examen(Long idUsuario, Long idNivel) throws SQLException {
        RespuestaJSON respuesta = usuarioSrv.existeIdUsuario(idUsuario);
        if (respuesta != null) {
            return respuesta;
        }
        respuesta = this.existeIdNivel(idNivel);
        if (respuesta != null) {
            return respuesta;
        }
        respuesta = new RespuestaJSON();
        Usuario u = usuarioDao.getUserById(idUsuario);
        Nivel n = cursoDao.getNivel(idNivel);
        List<Pregunta> preguntas = examenDao.crearExamen(idUsuario, n);
        Pregunta[] preguntaArr = new Pregunta[preguntas.size()];
        preguntaArr = preguntas.toArray(preguntaArr);
        respuesta.setEstado(true);
        respuesta.setMensaje("Examen creado");
        respuesta.setRespuesta(preguntaArr);
        return respuesta;
    }
}
