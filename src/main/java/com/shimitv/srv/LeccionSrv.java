/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.srv;

import com.shimitv.dao.LeccionDAO;
import com.shimitv.dao.PreguntaDAO;
import com.shimitv.dao.UsuarioDAO;
import com.shimitv.dom.Leccion;
import java.sql.SQLException;
import java.util.Map;

/**
 *
 * @author helio
 */
public class LeccionSrv {
    UsuarioDAO udao = new UsuarioDAO();
    LeccionDAO ldao = new LeccionDAO();
    PreguntaDAO pdao = new PreguntaDAO();
    public Leccion getLeccion(Long idLeccion){
        return ldao.getLeccion(idLeccion);
    }
    
    public Map<Long, String> getPreguntaLecciones(Long idLeccion) throws SQLException{
        return pdao.preguntaByLeccion(idLeccion);
    }   
 
    
    /*public Cuestionario getCuestionario(Map<Long, String> claves) throws SQLException{
        Cuestionario c = new Cuestionario();
        for(Long idPregunta: claves.keySet()){
            String tipoPregunta = claves.get(idPregunta);
            switch(tipoPregunta){
                case "ESCRITURA":
                    c.addPregunta(pdao.getPreguntaEscritura(idPregunta));
                    break;
                case "SELECCION":
                    c.addPregunta(pdao.getPreguntaOrdenamiento(idPregunta));
                    break;
                case "ORDENAR":
                    c.addPregunta(pdao.getPreguntaSeleccion(idPregunta));
                    break;
            }
        }
        return c;
    }*/
}
