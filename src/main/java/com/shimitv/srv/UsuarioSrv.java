/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.srv;

import com.shimitv.dao.CursoDAO;
import com.shimitv.dao.LeccionDAO;
import com.shimitv.dao.UsuarioDAO;
import com.shimitv.dom.Curso;
import com.shimitv.dom.Idioma;
import com.shimitv.dom.Leccion;
import com.shimitv.dom.LeccionCompletada;
import com.shimitv.dom.Nivel;
import com.shimitv.dom.RespuestaJSON;
import com.shimitv.dom.Usuario;
import static com.shimitv.dom.Usuario.validateEmail;
import com.shimitv.rep.IdiomaRep;
import com.shimitv.wsrv.UsuarioRest;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author Paola
 */
@Service
public class UsuarioSrv {

    @Autowired
    UsuarioDAO usuarioDao;
    @Autowired
    CursoDAO cursoDao;
    @Autowired
    LeccionDAO leccionDao;
    @Autowired
    IdiomaRep idiomaDao;
    @Autowired
    CursoSrv cursoSrv;
    
    private static final Logger logger = Logger.getLogger(UsuarioSrv.class.getName());

    public RespuestaJSON existeIdUsuario(Long idUsuario) throws SQLException {
        RespuestaJSON respuesta = new RespuestaJSON();
        boolean exists = usuarioDao.isUser(idUsuario);

        if (exists == false) {
            respuesta.setEstado(false);
            respuesta.setMensaje("No es usuario del sistema");
            respuesta.setRespuesta(false);
            return respuesta;
        }
        return null;
    }

    String mensaje = "";

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    
    public RespuestaJSON registrarUsuario(String nick, String email, String pw) throws SQLException {
        RespuestaJSON respuesta = new RespuestaJSON();
        Usuario existente;
        mensaje = "";
        if (usuarioDao.hasEmail(email)) {
            mensaje = "El correo ya está registrado en SHIMI TV";
        } else if (usuarioDao.hasUser(nick)) {
            mensaje += "El nick registrado ya existe en SHIMI TV";
        } else if (validateEmail(email)) {
            pw = passwordEncoder.encode(pw);
            if (usuarioDao.addUser(nick, email, pw)) {
                mensaje = "El usuario se ha agregado correctamente";
                respuesta.setEstado(true);
                respuesta.setMensaje(mensaje);
                respuesta.setRespuesta(true);
                return respuesta;
            } else {
                mensaje = "No se ha podido registrar el usuario";
            }
        } else {
            //   existente=new Usuario(null,nick,nombre,email,idioma,pw);
            mensaje = "Ingrese un correo válido ";
        }
        respuesta.setEstado(false);
        respuesta.setMensaje(mensaje);
        respuesta.setRespuesta(false);
        return respuesta;
    }

    public RespuestaJSON loginUsuario(String usuario, String pw) throws SQLException {
        RespuestaJSON respuesta = new RespuestaJSON();
        mensaje = "";
        if (usuarioDao.loguinUser(usuario, pw)) {
            mensaje = "El usuario ha ingresado al Sistema";
            respuesta.setEstado(true);
            respuesta.setMensaje(mensaje);
            respuesta.setRespuesta(true);
        } else {
            mensaje = "USUARIO/CONTRASEÑA invalidos";
            respuesta.setEstado(false);
            respuesta.setMensaje(mensaje);
            respuesta.setRespuesta(false);
        }
        System.out.println("Mensaje: "+mensaje);
        return respuesta;
    }

    public RespuestaJSON matricular(Long idUsuario, Long idCurso) throws SQLException {
        RespuestaJSON respuesta = this.existeIdUsuario(idUsuario);
        if (respuesta != null) {
            return respuesta;
        }
        respuesta = cursoSrv.existeIdCurso(idCurso);
        if (respuesta != null) {
            return respuesta;
        }
        mensaje = "";
        respuesta = new RespuestaJSON();
        boolean estado = usuarioDao.isMatriculado(idUsuario, idCurso);
        if (estado == false) {
            usuarioDao.matricular(idUsuario, idCurso);
            mensaje = "Matriculado con éxito";
            respuesta.setEstado(true);
            respuesta.setMensaje(mensaje);
            respuesta.setRespuesta(true);
        } else {
            mensaje = "El usuario ya está matriculado";
            respuesta.setEstado(false);
            respuesta.setMensaje(mensaje);
            respuesta.setRespuesta(false);
        }
        return respuesta;
    }

    //P@ola //cursos que puede tomar el usuario segun sus lenguas nativas
    public RespuestaJSON cursosDisponibles(Long idUsuario) throws SQLException {
        RespuestaJSON respuesta = this.existeIdUsuario(idUsuario);
        if (respuesta != null) {
            return respuesta;
        }
        respuesta = new RespuestaJSON();
        List<Curso> cursos = cursoDao.cursos();        
        Usuario usuarioActual = usuarioDao.getUserById(idUsuario);
        List<Curso> cursosMatriculados = usuarioActual.getCursos();
        cursos.removeAll(cursosMatriculados);

        if(cursosMatriculados.size() > 0){
            int i = 0;
            for(Curso c: cursosMatriculados){
                long idCurso = c.getIdCurso();
                List<Nivel> niveles = c.getNiveles();
                int j = 0;
                for(Nivel n: niveles){
                    int numLecciones = leccionDao.getLeccionByNivel(n.getIdNivel()).size();
                    int numLeccionesCompletadas = leccionDao.getLeccionesCompletadasByUsuarioAndByNivel(idUsuario, n.getIdNivel()).size();
                    logger.log(Level.SEVERE, String.valueOf(numLecciones));
                    niveles.get(j).setNumLecciones(numLecciones);
                    niveles.get(j).setNumLeccionesCompletadas(numLeccionesCompletadas);
                    logger.log(Level.SEVERE,"Niveles: " + String.valueOf(niveles.get(0).getNumLecciones()) + "NivelesCompletados: " + String.valueOf(niveles.get(0).getNumLeccionesCompletadas())); 
                    j++;
                }
                if(niveles.size() > 0){
                   logger.log(Level.SEVERE,"Niveles: " + String.valueOf(niveles.get(0).getNumLecciones()) + "NivelesCompletados: " + String.valueOf(niveles.get(0).getNumLeccionesCompletadas())); 
                }
                
                cursosMatriculados.get(i).setNiveles(niveles);
                i++;
            }
        }
        List<List<Curso>> listaCursos = new ArrayList<>();
        listaCursos.add(cursos);
        listaCursos.add(cursosMatriculados);
        
        respuesta.setEstado(true);
        respuesta.setMensaje("Éxito");
        respuesta.setRespuesta(listaCursos);
        return respuesta;
    }

    @Autowired
    ManejadorArchivos mArchivo;
    public RespuestaJSON getleccionesCompletadas(Long idUsuario, Long idNivel) throws SQLException{
        RespuestaJSON respuesta = this.existeIdUsuario(idUsuario);
        if (respuesta != null) {
            return respuesta;
        }
        respuesta = new RespuestaJSON();
        List<LeccionCompletada> leccionesC = new ArrayList<LeccionCompletada>();
        List<Long> lecciones = new ArrayList<Long>();
        leccionesC=leccionDao.getLeccionesCompletadasByNivel(idNivel);
        for(LeccionCompletada lec: leccionesC ){
            lecciones.add(lec.getIdLeccion());
        }
       
        Usuario usuarioActual = usuarioDao.getUserById(idUsuario);
        respuesta.setEstado(true);
        respuesta.setMensaje("Éxito");
        respuesta.setRespuesta(lecciones);
        return respuesta;
    }
    
    
  

    public RespuestaJSON terminarLeccion(Long idUsuario, Long idLeccion) throws SQLException {
        RespuestaJSON respuesta = this.existeIdUsuario(idUsuario);
        if (respuesta != null) {
            return respuesta;
        }
        respuesta = cursoSrv.existeIdLeccion(idLeccion);
        if (respuesta != null) {
            //No existe la leccion
            return respuesta;
        }
        respuesta = new RespuestaJSON();
        boolean isCorrecto = leccionDao.registrarLeccionTerminada(idLeccion, idUsuario);
        if (isCorrecto == false) {
            respuesta.setEstado(false);
            respuesta.setMensaje("Repaso toda la lección con exito");
            respuesta.setRespuesta(false);
        }
        respuesta.setEstado(true);
        respuesta.setMensaje("Ha terminado la lección satisfactoriamente");
        respuesta.setRespuesta(true);
        
        Leccion l=leccionDao.getLeccion(idLeccion);
        //matricular despues de la primera leccion
        if(l.getNumLeccion()==1 && l.getNivel().getOrdenNivel()==1 && !usuarioDao.isMatriculado(idUsuario, l.getCurso().getIdCurso())){
            RespuestaJSON r=this.matricular(idUsuario, l.getCurso().getIdCurso());
            if(r.isEstado()){
                respuesta.setMensaje(respuesta.getMensaje()+"\n Ud ha completado su matricula en este curso\n Felicidades!!!");
            }
        }
        return respuesta;
    }
    
    //Paola: obtiene lista de idiomas (objetos) que habla usuario
    public RespuestaJSON getIdiomasUsuario(Long idUsuario) throws SQLException{
        RespuestaJSON respuesta = new RespuestaJSON();
        Usuario user=usuarioDao.getUserById(idUsuario);
        if (user!=null) {
            respuesta.setEstado(true);
            respuesta.setMensaje("Éxito");
            respuesta.setRespuesta(user.getIdiomas());
            
        }
        return respuesta;
    }
    
    //Devuelve todos los idiomas
    public RespuestaJSON get_idiomas(){
        RespuestaJSON respuesta = new RespuestaJSON();
         respuesta.setEstado(true);
            respuesta.setMensaje("Éxito");
            respuesta.setRespuesta(cursoDao.idiomas());
        return respuesta;
    }

    //Paola: obtiene el id del la ultima leccion que hizo el usuario
    public RespuestaJSON getUltimaLecciondeUsuario(Long idUsuario, Long idNivel){
        RespuestaJSON respuesta = new RespuestaJSON();
        try {
            LeccionCompletada leccion=leccionDao.getLastLeccion(idUsuario, idNivel);
            if (leccion!=null) {
            respuesta.setEstado(true);
            respuesta.setMensaje("Éxito");
            respuesta.setRespuesta(leccion);
            
        }
        } catch (SQLException ex) {
            respuesta.setEstado(false);
            respuesta.setMensaje("No existe una ultima leccion");
            respuesta.setRespuesta(false);
        }
        
        return respuesta;
    }
    
    //detalles de cerrar sesion (por si se requieren a futuro)
    public RespuestaJSON cerrarSesiondeUsuario(Long idUsuario){
         RespuestaJSON respuesta = new RespuestaJSON();
         
         return respuesta;
    }

    

    public RespuestaJSON getUsuarioId(Authentication authentication) {
        Usuario u = usuarioDao.getUserByNick(authentication.getName());
        RespuestaJSON respuesta = new RespuestaJSON();
        if(u == null){
            respuesta.setEstado(false);
            respuesta.setMensaje("Acceso no autorizado");
            respuesta.setRespuesta(false);      
        }else{
            respuesta.setEstado(true);
            respuesta.setMensaje("Éxito");
            respuesta.setRespuesta(u.getIdUsuario());
        }
        return respuesta;
    }
    
    public RespuestaJSON getUsuario(Authentication authentication) {
        Usuario u = usuarioDao.getUserByNick(authentication.getName());
        RespuestaJSON respuesta = new RespuestaJSON();
        if(u == null){
            respuesta.setEstado(false);
            respuesta.setMensaje("Acceso no autorizado");
            respuesta.setRespuesta(false);      
        }else{
            respuesta.setEstado(true);
            respuesta.setMensaje("Éxito");
            respuesta.setRespuesta(u);
        }
        return respuesta;
    }
}
