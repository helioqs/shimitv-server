/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.srv;

import com.shimitv.rep.UsuarioRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author helio
 */
@Service("userDetailsService")
public class UsuarioAuthSrv implements UserDetailsService{
    @Autowired
    private UsuarioRep urep;
    
    @Override
    public UserDetails loadUserByUsername(String string) throws UsernameNotFoundException {
        return urep.findOneByNickname(string);
    } 
}
