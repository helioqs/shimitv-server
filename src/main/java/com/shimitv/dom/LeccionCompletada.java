/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dom;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author helio
 */
@Entity
public class LeccionCompletada implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="lcomp_seq")
    @SequenceGenerator(name="lcomp_seq", sequenceName="lcomp_seq")
    private Long idLeccionCompletada;
    private Long idUsuario;
    private Long idLeccion;
    @OneToOne
    @JoinColumn(name="idCurso")
    @JsonIgnore
    private Curso curso;

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    public LeccionCompletada(Long idUsuario, Long idLeccion, Curso curso) {
        this.idUsuario = idUsuario;
        this.idLeccion = idLeccion;
        this.curso = curso;
        this.fecha = new Date();
    }
    

    public LeccionCompletada() {
    }
    
    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getIdLeccion() {
        return idLeccion;
    }

    public void setIdLeccion(Long idLeccion) {
        this.idLeccion = idLeccion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getIdLeccionCompletada() {
        return idLeccionCompletada;
    }

    public void setIdLeccionCompletada(Long idLeccionCompletada) {
        this.idLeccionCompletada = idLeccionCompletada;
    }

    
   
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.idUsuario);
        hash = 97 * hash + Objects.hashCode(this.idLeccion);
        hash = 97 * hash + Objects.hashCode(this.curso.getIdCurso());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LeccionCompletada other = (LeccionCompletada) obj;
        if (!Objects.equals(this.idUsuario, other.idUsuario)) {
            return false;
        }
        if (!Objects.equals(this.idLeccion, other.idLeccion)) {
            return false;
        }
        if (!Objects.equals(this.curso.getIdCurso(), other.curso.getIdCurso())) {
            return false;
        }
        return true;
    }
    
    
}
