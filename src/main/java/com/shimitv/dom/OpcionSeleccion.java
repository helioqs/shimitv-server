/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dom;

import java.io.Serializable;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author helio
 */
@Embeddable
@Table(name="opcionesSeleccion")
public class OpcionSeleccion  implements Serializable{
    private String opcionTexto = "";
    private String opcionImg = "";
    private boolean correcto = false;

    public OpcionSeleccion() {
    }

    
    
    public OpcionSeleccion(String opcionTexto) {
        this.opcionTexto = opcionTexto;
    }
    
    public OpcionSeleccion(String opcionTexto, String opcionImg) {
        this.opcionTexto = opcionTexto;
        this.opcionImg = opcionImg;
    }
    
    public String getOpcionTexto() {
        return opcionTexto;
    }

    public void setOpcionTexto(String opcionTexto) {
        this.opcionTexto = opcionTexto;
    }

    public String getOpcionImg() {
        return opcionImg;
    }

    public void setOpcionImg(String opcionImg) {
        this.opcionImg = opcionImg;
    }

    public boolean isCorrecto() {
        return correcto;
    }

    public void setCorrecto(boolean correcto) {
        this.correcto = correcto;
    }
    
    
}
