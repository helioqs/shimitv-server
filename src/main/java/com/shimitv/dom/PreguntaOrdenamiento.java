/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dom;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;

/**
 *
 * @author helio
 */
@Entity
public class PreguntaOrdenamiento extends Pregunta{
    @ElementCollection
    @CollectionTable(name = "opciones")
    private List<String> opciones = new ArrayList<>();
    @Column(nullable=false)
    private String respuesta;
    public PreguntaOrdenamiento( Long idLeccion, Long numPregunta, String tipo, String enunciado, String urlAudio, String retroalimentacion, String respuesta) {
        super( idLeccion, numPregunta, tipo, enunciado, urlAudio, retroalimentacion);
        this.respuesta = respuesta;
    }    

    public PreguntaOrdenamiento() {
    }
    
    public List<String> getOpciones() {
        return opciones;
    }

    public String getRespuesta() {
        return respuesta;
    }
    
    public void addRespuesta(String opcion){
        opciones.add(opcion);
    }    

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
}
