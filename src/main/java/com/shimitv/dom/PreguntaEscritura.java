/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dom;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;

/**
 *
 * @author helio
 */
@Entity
public class PreguntaEscritura extends Pregunta{
    @ElementCollection
    @CollectionTable(name = "respuestas")
    private List<String> respuesta = new LinkedList<>();
    public PreguntaEscritura(Long idLeccion, Long numPregunta, String tipo, String enunciado, String urlAudio, String retroalimentacion) {
        super(idLeccion, numPregunta, tipo, enunciado, urlAudio, retroalimentacion);
    }

    public PreguntaEscritura() {
    }
    
    public void addRespuesta(String opcion){
        respuesta.add(opcion);
    }

    public List<String> getRespuesta() {
        return respuesta;
    }    
}
