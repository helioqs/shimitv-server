/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dom;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

/**
 *
 * @author helio
 */
@Entity
public class Curso {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="curso_seq")
    @SequenceGenerator(name="curso_seq", sequenceName="cursos_seq")
    private Long idCurso;
    @OneToOne
    @JoinColumn(name="idiomaOrigen")
    private Idioma idiomaOrigen;
    @OneToOne
    @JoinColumn(name="idiomaEstudio")
    private Idioma idiomaEstudio;
    private String imagen;
    private String imagenBandera;
    @ManyToMany
    @JoinTable(
      name="Curso_niveles",
      joinColumns=@JoinColumn(name="idCurso", referencedColumnName="idCurso"),
      inverseJoinColumns=@JoinColumn(name="idNivel", referencedColumnName="idNivel"))
    private List<Nivel> niveles;

    public Curso(Long idCurso, String imagen) {
        this.idCurso = idCurso;
        this.imagen = imagen;
        this.niveles = new ArrayList<>();
    }

    public Curso() {
        this.niveles = new ArrayList<>();
    }    
    
    public Long getIdCurso() {
        return idCurso;
    }

    public String getImagen() {
        return imagen;
    }

    public void setIdCurso(Long idCurso) {
        this.idCurso = idCurso;
    }


    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public List<Nivel> getNiveles() {
        return niveles;
    }

    public void setNiveles(List<Nivel> niveles) {
        this.niveles = niveles;
    }
    
    public void addNivel(Nivel n) {
        this.niveles.add(n);
    }

    public Idioma getIdiomaOrigen() {
        return idiomaOrigen;
    }

    public void setIdiomaOrigen(Idioma idiomaOrigen) {
        this.idiomaOrigen = idiomaOrigen;
    }

    public Idioma getIdiomaEstudio() {
        return idiomaEstudio;
    }

    public void setIdiomaEstudio(Idioma idiomaEstudio) {
        this.idiomaEstudio = idiomaEstudio;
    }  

    public String getImagenBandera() {
        return imagenBandera;
    }

    public void setImagenBandera(String imagenBandera) {
        this.imagenBandera = imagenBandera;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.idCurso);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Curso other = (Curso) obj;
        if (!Objects.equals(this.idCurso, other.idCurso)) {
            return false;
        }
        return true;
    }
    
    
}
