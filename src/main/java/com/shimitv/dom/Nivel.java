/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dom;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

/**
 *
 * @author helio
 */
@Entity
public class Nivel implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="nivel_seq")
    @SequenceGenerator(name="nivel_seq", sequenceName="nivel_seq")
    private Long idNivel;
    @Column(nullable=false)
    private int ordenNivel;
    @Column(nullable=false)
    private String descripcion;
    @ManyToOne
    @JoinColumn(name="idCurso")
    @JsonIgnore
    private Curso curso;
    @Transient
    private int numLecciones;
    @Transient
    private int numLeccionesCompletadas;

    public int getNumLecciones() {
        return numLecciones;
    }

    public void setNumLecciones(int numLecciones) {
        this.numLecciones = numLecciones;
    }

    public int getNumLeccionesCompletadas() {
        return numLeccionesCompletadas;
    }

    public void setNumLeccionesCompletadas(int numLeccionesCompletadas) {
        this.numLeccionesCompletadas = numLeccionesCompletadas;
    }

    @JsonProperty("idCurso")
    public Long getCursoId(){
        return this.curso.getIdCurso();
    }
    
    public Long getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(Long idNivel) {
        this.idNivel = idNivel;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Nivel(String descripcion) {
        this.descripcion = descripcion;
    }

    public Nivel() {
    }

    public int getOrdenNivel() {
        return ordenNivel;
    }

    public void setOrdenNivel(int ordenNivel) {
        this.ordenNivel = ordenNivel;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.idNivel);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Nivel other = (Nivel) obj;
        if (!Objects.equals(this.idNivel, other.idNivel)) {
            return false;
        }
        return true;
    }    
}
