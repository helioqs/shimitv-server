/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dom;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

/**
 *
 * @author Paola
 */
@Entity
public class Leccion {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="leccion_seq")
    @SequenceGenerator(name="leccion_seq", sequenceName="leccion_seq")
    private Long idLeccion;
    @ManyToOne
    @JoinColumn(name="idNivel")
    private Nivel nivel;
    @ManyToOne
    @JoinColumn(name="idCurso")
    private Curso curso;
    @Column(nullable=false)
    private int numLeccion;
    private String tema;
    private String descripcion;
    private String urlimagen;
    private int ultimaLeccion;
    @Transient
    private boolean terminado;

    public Leccion(Long idLeccion, int numLeccion, String tema, String descripcion, String urlimagen, int ultimaLeccion) {
        this.idLeccion = idLeccion;
        this.numLeccion = numLeccion;
        this.tema = tema;
        this.descripcion = descripcion;
        this.urlimagen = urlimagen;
        this.ultimaLeccion = ultimaLeccion;
        this.terminado = false;
    }

    public boolean isTerminado() {
        return terminado;
    }

    public void setTerminado(boolean terminado) {
        this.terminado = terminado;
    }

    public Leccion() {
    }

    
    
    public Long getIdLeccion() {
        return idLeccion;
    }

    public void setIdLeccion(Long idLeccion) {
        this.idLeccion = idLeccion;
    }

    public int getNumLeccion() {
        return numLeccion;
    }

    public void setNumLeccion(int numLeccion) {
        this.numLeccion = numLeccion;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUrlimagen() {
        return urlimagen;
    }

    public void setUrlimagen(String urlimagen) {
        this.urlimagen = urlimagen;
    }

    public int getUltimaLeccion() {
        return ultimaLeccion;
    }

    public void setUltimaLeccion(int ultimaLeccion) {
        this.ultimaLeccion = ultimaLeccion;
    } 

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.idLeccion);
        return hash;
    }

    public Nivel getNivel() {
        return nivel;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Leccion other = (Leccion) obj;
        if (!Objects.equals(this.idLeccion, other.idLeccion)) {
            return false;
        }
        return true;
    }
    
    
}
