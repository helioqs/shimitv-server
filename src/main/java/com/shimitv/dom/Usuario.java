/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dom;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author helio
 */
@Entity
public class Usuario implements UserDetails {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator="usuario_seq")
    @SequenceGenerator(name="usuario_seq", sequenceName="usuarios_seq")
    @JsonIgnore
    private Long idUsuario;
    @Column(unique=true, length = 90, nullable=false)
    private String nickname;
    @Column(unique=true, length = 90, nullable=false)
    private String email;
    @ManyToMany
    @JoinTable(
      name="Usuario_idiomas",
      joinColumns=@JoinColumn(name="idUsuario", referencedColumnName="idUsuario"),
      inverseJoinColumns=@JoinColumn(name="idIdioma", referencedColumnName="idIdioma"))
    @JsonIgnore
    private List<Idioma> idiomas; 
    @Column(name = "activo", nullable = false)
    private boolean activo; 
    @JsonIgnore
    @ManyToMany
    @JoinTable(
      name="Usuario_cursos",
      joinColumns=@JoinColumn(name="idUsuario", referencedColumnName="idUsuario"),
      inverseJoinColumns=@JoinColumn(name="idCurso", referencedColumnName="idCurso"))
    private List<Curso> cursos = new ArrayList<>();
    @JsonIgnore
    private String password;
   
    
    public Usuario(){
        this.idiomas = new ArrayList<>();
        this.activo = true;
    }

    public Usuario(String nick, String mail, String pass) {
        this.nickname = nick;
        this.email = mail;
        this.idiomas = new ArrayList<>();
        this.password = pass;
        this.idiomas = new ArrayList<>();
        this.activo = true;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Idioma> getIdiomas() {
        return idiomas;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setIdiomas(List<Idioma> idiomas) {
        this.idiomas = idiomas;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.idUsuario);
        hash = 31 * hash + Objects.hashCode(this.nickname);
        hash = 31 * hash + Objects.hashCode(this.email);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.nickname, other.nickname)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.idUsuario, other.idUsuario)) {
            return false;
        }
        return true;
    }

    public List<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }
    
    public void addCurso(Curso c){
        this.cursos.add(c);
    }
    
    public void addIdioma(Idioma i){
        this.idiomas.add(i);
    }
    
    //Validar los email de usuario
    private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    
    public static boolean validateEmail(String email) {
        // Compiles the given regular expression into a pattern.
        Pattern pattern = Pattern.compile("^([0-9a-zA-Z]([_.w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-w]*[0-9a-zA-Z].)+([a-zA-Z]{2,9}.)+[a-zA-Z]{2,3})$");
 
        // Match the given input against this pattern
        Matcher matcher1 = pattern.matcher(email);
        
       if(matcher1.find()==true){
                 System.out.println("Correcto: "+email);
                 return true;}
       else {
            System.out.println("Error: "+email);
               return false;
       }
 
    }

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();	
        return authorities;
    }

    @Override
    @JsonIgnore
    public String getUsername() {
        return nickname;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return activo;
    }

    @JsonIgnore
    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
    
    
}
