/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author helio
 */
@Entity
@Table(name="PreguntaSeleccionMultiple")
public class PreguntaSeleccion extends Pregunta implements Serializable{
    @ElementCollection
    @CollectionTable(name = "opcionesSeleccion")
    private List<OpcionSeleccion> opciones = new ArrayList<>();
    public PreguntaSeleccion(Long idLeccion, Long numPregunta, String tipo, String enunciado, String urlAudio, String retroalimentacion) {
        super(idLeccion, numPregunta, tipo, enunciado, urlAudio, retroalimentacion);
    }

    public PreguntaSeleccion() {
    }
    
    public void addOpcionTexto(String opcionTexto){
        OpcionSeleccion o = new OpcionSeleccion(opcionTexto);
        opciones.add(o);
    }
    
    public void addOpcionImg(String opcionTexto, String opcionImagen){
        OpcionSeleccion o = new OpcionSeleccion(opcionTexto, opcionImagen);
        opciones.add(o);
    }

    public List<OpcionSeleccion> getOpciones() {
        return opciones;
    }

    public void setOpciones(List<OpcionSeleccion> opciones) {
        this.opciones = opciones;
    }
}
