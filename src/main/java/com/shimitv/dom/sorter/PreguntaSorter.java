/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dom.sorter;

import com.shimitv.dom.Pregunta;
import java.util.Comparator;

/**
 *
 * @author helio
 */
public class PreguntaSorter implements Comparator<Pregunta>{

    @Override
    public int compare(Pregunta o1, Pregunta o2) {
        return o1.getNumPregunta().compareTo(o2.getNumPregunta());
    }
    
}
