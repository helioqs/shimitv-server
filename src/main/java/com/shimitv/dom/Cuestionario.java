/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dom;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author helio
 */
public class Cuestionario {
    private List<Pregunta> preguntas;

    public Cuestionario(){
        preguntas = new ArrayList<>();
    }
    
    public List<Pregunta> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(List<Pregunta> preguntas) {
        this.preguntas = preguntas;
    }
    
    public void addPregunta(Pregunta p){
        this.preguntas.add(p);
    }
}
