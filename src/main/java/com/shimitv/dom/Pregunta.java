/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dom;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.TableGenerator;

/**
 *
 * @author helio
 */
@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY , property = "type")
@JsonSubTypes({
    @Type(value = PreguntaEscritura.class, name = "PreguntaEscritura"),
    @Type(value = PreguntaOrdenamiento.class, name = "PreguntaOrdenamiento"),
    @Type(value = PreguntaSeleccion.class, name = "PreguntaSeleccion") }
)
public abstract class Pregunta implements Serializable {
    @Id
    @TableGenerator(name="pregunta_seq", table="sequence_table", pkColumnName="seq_name",
        valueColumnName="seq_count", pkColumnValue="emp_seq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="pregunta_seq")
    private Long idPregunta;
    private Long idLeccion;
    private Long numPregunta;
    private String tipo;
    private String enunciado;
    private String urlAudio;
    private String retroalimentacion;

    public Pregunta(Long idLeccion, Long numPregunta, String tipo, String enunciado, String urlAudio, String retroalimentacion) {
        this.idLeccion = idLeccion;
        this.numPregunta = numPregunta;
        this.tipo = tipo;
        this.enunciado = enunciado;
        this.urlAudio = urlAudio;
        this.retroalimentacion = retroalimentacion;
    }

    public Pregunta() {
    }

    public Long getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(Long idPregunta) {
        this.idPregunta = idPregunta;
    }

    public Long getIdLeccion() {
        return idLeccion;
    }

    public void setIdLeccion(Long idLeccion) {
        this.idLeccion = idLeccion;
    }

    public Long getNumPregunta() {
        return numPregunta;
    }

    public void setNumPregunta(Long numPregunta) {
        this.numPregunta = numPregunta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public String getUrlAudio() {
        return urlAudio;
    }

    public void setUrlAudio(String urlAudio) {
        this.urlAudio = urlAudio;
    }

    public String getRetroalimentacion() {
        return retroalimentacion;
    }

    public void setRetroalimentacion(String retroalimentacion) {
        this.retroalimentacion = retroalimentacion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.idPregunta);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pregunta other = (Pregunta) obj;
        if (!Objects.equals(this.idPregunta, other.idPregunta)) {
            return false;
        }
        return true;
    }    
}
