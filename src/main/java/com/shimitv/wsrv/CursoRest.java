/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.wsrv;

import com.shimitv.dom.RespuestaJSON;
import com.shimitv.srv.CursoSrv;
import com.shimitv.srv.UsuarioSrv;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author helio
 */
@RestController
public class CursoRest {
    @Autowired
    CursoSrv cursoSrv;
    @Autowired
    UsuarioSrv usuarioSrv;
    private static final Logger logger = Logger.getLogger(UsuarioRest.class.getName());
    
    @RequestMapping(value = "/get_nivel_curso", method = RequestMethod.GET)
    public RespuestaJSON get_nivel_curso(@RequestParam Long idCurso) throws SQLException{
        RespuestaJSON respuesta = cursoSrv.nivel_curso(idCurso); 
        logger.log(Level.INFO, "Resultado /get_nivel_curso : ["+ respuesta.getMensaje() + "] --- Consulta: /get_nivel_curso?idCurso="+idCurso);
        return respuesta;
    }
    
    @RequestMapping(value = "/get_lecciones_nivel", method = RequestMethod.GET)
    public RespuestaJSON get_lecciones_nivel(Authentication authentication, @RequestParam Long idNivel) throws SQLException{
        RespuestaJSON respuestaUsuarioId = usuarioSrv.getUsuarioId(authentication);
        if(!respuestaUsuarioId.isEstado()){
            return respuestaUsuarioId;
        }
        Long idUsuario = (Long) respuestaUsuarioId.getRespuesta();
        RespuestaJSON respuesta = cursoSrv.lecciones_nivel(idUsuario, idNivel);
        logger.log(Level.INFO, "Resultado /get_lecciones_nivel : ["+ respuesta.getMensaje() + "] --- Consulta: /get_lecciones_nivel?idNivel="+idNivel);
        return respuesta;
    }
    
    @RequestMapping(value = "/get_cuestionario_leccion", method = RequestMethod.GET)
    public RespuestaJSON get_cuestionario_leccion(@RequestParam Long idLeccion) throws SQLException{
        RespuestaJSON respuesta = cursoSrv.get_cuestionario_leccion(idLeccion);
        logger.log(Level.INFO, "Resultado /get_cuestionario_leccion : ["+ respuesta.getMensaje() + "] --- Consulta: /get_cuestionario_leccion?idLeccion="+idLeccion);
        return respuesta;
    }
    
    @RequestMapping(value = "/get_examen", method = RequestMethod.GET)
    public RespuestaJSON get_examen(Authentication authentication,
                                    @RequestParam Long idNivel) throws SQLException{
        RespuestaJSON respuestaUsuarioId = usuarioSrv.getUsuarioId(authentication);
        if(!respuestaUsuarioId.isEstado()){
            return respuestaUsuarioId;
        }
        Long idUsuario = (Long) respuestaUsuarioId.getRespuesta();
        RespuestaJSON respuesta = cursoSrv.get_examen(idUsuario, idNivel);
        logger.log(Level.INFO, "Resultado /get_examen : ["+ respuesta.getMensaje() + "] --- Consulta: /get_examen?idNivel="+idNivel+"&idUsuario="+idUsuario);
        return respuesta;
    }
    
    @RequestMapping(value = "/save_examen", method = RequestMethod.GET)
    public RespuestaJSON save_examen(Authentication authentication, 
                                    @RequestParam Long idNivel,
                                    @RequestParam Long idCurso,
                                    @RequestParam String calificacion) throws SQLException{
        RespuestaJSON respuestaUsuarioId = usuarioSrv.getUsuarioId(authentication);
        if(!respuestaUsuarioId.isEstado()){
            return respuestaUsuarioId;
        }
        Long idUsuario = (Long) respuestaUsuarioId.getRespuesta();
        RespuestaJSON respuesta = cursoSrv.save_examen(idUsuario,idNivel,idCurso,calificacion);
        logger.log(Level.INFO, "Resultado /save_examen : ["+ respuesta.getMensaje() + "] --- Consulta: /save_examen?idUsuario="+idUsuario+"&idNivel="+idNivel+"&idCurso="+idCurso+"&calificacion="+calificacion);
        return respuesta;
    }
    
    @RequestMapping(value = "/get_examen_resultados", method = RequestMethod.GET)
    public RespuestaJSON get_examen_resultados(Authentication authentication, 
                                               @RequestParam Long idNivel) throws SQLException{
        RespuestaJSON respuestaUsuarioId = usuarioSrv.getUsuarioId(authentication);
        if(!respuestaUsuarioId.isEstado()){
            return respuestaUsuarioId;
        }
        Long idUsuario = (Long) respuestaUsuarioId.getRespuesta();
        RespuestaJSON respuesta = cursoSrv.get_examen_resultados(idNivel, idUsuario);
        logger.log(Level.INFO, "Resultado /get_examen_resultados : ["+ respuesta.getMensaje() + "] --- Consulta: /get_examen_resultados?idNivel="+idNivel+"&idUsuario="+idUsuario);
        return respuesta;
    }
    
    
}
