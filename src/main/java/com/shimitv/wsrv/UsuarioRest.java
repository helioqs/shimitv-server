/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.wsrv;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import com.shimitv.dao.LeccionDAO;
import com.shimitv.rep.CursoRep;
import com.shimitv.dao.UsuarioDAO;
import com.shimitv.dom.RespuestaJSON;
import java.sql.SQLException;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.shimitv.rep.IdiomaRep;
import com.shimitv.srv.ManejadorArchivos;
import com.shimitv.srv.UsuarioSrv;
import java.io.IOException;
import java.util.logging.Level;
import javax.annotation.Resource;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
/**
 *
 * @author helio
 */
@RestController
public class UsuarioRest {
    @Autowired
    private IdiomaRep idiomaDAO;
    @Autowired
    private UsuarioDAO udao;
    @Autowired
    private CursoRep cursoDAO;
    @Autowired
    LeccionDAO ldao;
    @Autowired
    UsuarioSrv usuarioSrv;
    
    private static final Logger logger = Logger.getLogger(UsuarioRest.class.getName());
    //Solicitud del usuario y método    
    @RequestMapping(value = "/crear_usuario", method = RequestMethod.GET)
    public RespuestaJSON crear_usuario(@RequestParam String nickname, 
                                 @RequestParam String email,
                                 @RequestParam String password) throws SQLException{
        RespuestaJSON isCreado = usuarioSrv.registrarUsuario(nickname, email, password);
        logger.log(Level.INFO, "Resultado /crear_usuario : ["+ isCreado.getMensaje() + "] --- Consulta: /crear_usuario?nickname="+nickname+"&email="+ email);
        return isCreado;
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public RespuestaJSON login(@RequestParam String usuario, 
                                 @RequestParam String password) throws SQLException{
        RespuestaJSON isCreado = usuarioSrv.loginUsuario(usuario, password);
        logger.log(Level.INFO, "Resultado /login : ["+ isCreado.getMensaje() + "] --- Consulta: /login?usuario="+usuario);
        return isCreado;
    }    
     
    @RequestMapping(value = "/matricular", method = RequestMethod.GET)
    public RespuestaJSON matricular(Authentication authentication, 
                                 @RequestParam Long idCurso) throws SQLException{
        RespuestaJSON respuestaUsuarioId = usuarioSrv.getUsuarioId(authentication);
        if(!respuestaUsuarioId.isEstado()){
            return respuestaUsuarioId;
        }
        Long idUsuario = (Long) respuestaUsuarioId.getRespuesta();
        RespuestaJSON matricular = usuarioSrv.matricular(idUsuario, idCurso);
        logger.log(Level.INFO, "Resultado /matricular : ["+ matricular.getMensaje() + "] --- Consulta: /matricular?idUsuario="+idUsuario+"&idCurso="+idCurso);
        return matricular;
    }  
    
    @RequestMapping(value = "/get_cursos_disponibles", method = RequestMethod.GET)
    public RespuestaJSON get_cursos_disponibles(Authentication authentication) throws SQLException{
        RespuestaJSON respuestaUsuarioId = usuarioSrv.getUsuarioId(authentication);
        if(!respuestaUsuarioId.isEstado()){
            return respuestaUsuarioId;
        }
        Long idUsuario = (Long) respuestaUsuarioId.getRespuesta();
        RespuestaJSON respuesta = usuarioSrv.cursosDisponibles(idUsuario);
        logger.log(Level.INFO, "Resultado /get_cursos_disponibles : ["+ respuesta.getMensaje() + "] --- Consulta: /get_cursos_disponibles?idUsuario="+idUsuario);
        return respuesta;
    }
    //Paola: Obtener Lecciones completadas
    public RespuestaJSON get_lecciones_terminadas(Long idUsuario, Long idNivel) throws SQLException{
        RespuestaJSON respuesta = usuarioSrv.getleccionesCompletadas(idUsuario, idNivel);
        logger.log(Level.INFO,"Resultado/get_lecciones_termnadas : ["+respuesta.getMensaje() +"] --- Consulta: /get_lecciones_terminadas");
        return respuesta;
    }
    //Paola: obtiene lista de todos los idiomas de la BD (objetos)
    @RequestMapping(value = "/get_idiomas", method = RequestMethod.GET)
    public RespuestaJSON get_idiomas() throws SQLException{
        RespuestaJSON respuesta = usuarioSrv.get_idiomas();
        logger.log(Level.INFO, "Resultado /get_idiomas : ["+ respuesta.getMensaje() + "] --- Consulta: /get_idiomas");
        return respuesta;
    }
    
    //Paola
    @RequestMapping(value = "/get_idiomas_usuario", method = RequestMethod.GET)
    public RespuestaJSON get_idiomas_de_usuario(Long idUsuario) throws SQLException{
        RespuestaJSON respuesta = usuarioSrv.getIdiomasUsuario(idUsuario);
        logger.log(Level.INFO, "Resultado /get_idiomas_usuario : ["+ respuesta.getMensaje() + "] --- Consulta: /get_idiomas");
        return respuesta;
    }
    
    @Resource(name="tokenServices")
    ConsumerTokenServices tokenServices;
    
    //Paola    
    @RequestMapping(value = "/salir", method = RequestMethod.GET)
    public  RespuestaJSON salir(Authentication authentication) throws SQLException{
        logger.log(Level.INFO, "Resultado /logout : ["+ authentication.getCredentials() + "]");
        RespuestaJSON respuestaUsuarioId = usuarioSrv.getUsuarioId(authentication);
        if(!respuestaUsuarioId.isEstado()){
            return respuestaUsuarioId;
        }
        RespuestaJSON r = new RespuestaJSON();
        r.setMensaje("Éxito");
        r.setEstado(true);
        r.setRespuesta(true);          
        String token = ((OAuth2AuthenticationDetails) authentication.getDetails()).getTokenValue();
        tokenServices.revokeToken(token);
        return r;
    }
    
    
    
    @RequestMapping(value = "/terminar_leccion", method = RequestMethod.GET)
    public RespuestaJSON terminar_leccion(Authentication authentication, Long idLeccion) throws SQLException{
        RespuestaJSON respuestaUsuarioId = usuarioSrv.getUsuarioId(authentication);
        if(!respuestaUsuarioId.isEstado()){
            return respuestaUsuarioId;
        }
        Long idUsuario = (Long) respuestaUsuarioId.getRespuesta();
        RespuestaJSON respuesta = usuarioSrv.terminarLeccion(idUsuario, idLeccion);
        logger.log(Level.INFO, "Resultado /terminar_leccion : ["+ respuesta.getMensaje() + "] --- Consulta: /terminar_leccion?idUsuario="+idUsuario+"&idLeccion="+idLeccion);
        return respuesta;
    }
    
    @RequestMapping(value = "/get_ultima_leccion_nivel", method = RequestMethod.GET)
    public RespuestaJSON get_ultima_leccion(Authentication authentication, @RequestParam long idNivel) throws SQLException{
        RespuestaJSON respuestaUsuarioId = usuarioSrv.getUsuarioId(authentication);
        if(!respuestaUsuarioId.isEstado()){
            return respuestaUsuarioId;
        }
        Long idUsuario = (Long) respuestaUsuarioId.getRespuesta();
        RespuestaJSON respuesta = usuarioSrv.getUltimaLecciondeUsuario(idUsuario, idNivel);
        logger.log(Level.INFO, "Resultado /get_ultima_leccion : ["+ respuesta.getMensaje() + "] --- Consulta: /get_ultima_leccion?idUsuario="+authentication.getName()+"&idNivel="+String.valueOf(idNivel));
        return respuesta;
    }
    
    @RequestMapping(value = "/userinfo", method = RequestMethod.GET)
    @ResponseBody
    public RespuestaJSON currentUserName(Authentication authentication) {
        RespuestaJSON respuestaUsuarioId = usuarioSrv.getUsuarioId(authentication);
        if(!respuestaUsuarioId.isEstado()){
            return respuestaUsuarioId;
        }
        RespuestaJSON respuesta = usuarioSrv.getUsuario(authentication);
        return respuesta;
    }
    
    
    @Autowired
    ManejadorArchivos mau;
    @RequestMapping(value = "/audio", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getAudio(@RequestParam String id) {
         byte[] audio=new byte[0];
        boolean estado = true;
        mau.comprbarExisteFicheroAudio(id);
        try {
            audio = mau.descargaAudio(id);
            //System.out.println("Estado=true");
            //System.out.println("/nAduio="+audio);
            estado =true;
        
        } catch (IOException ex) {
            audio = new byte[0];
            System.out.println("Estado=FALSO "+ex);
           // System.out.println("/nAduio="+audio);
            estado = false;
        }
       // MediaType media=new MediaType("audio/mpeg",".mp3");
        logger.log(Level.INFO, "Resultado /audio : ["+ estado + "] --- Consulta: /audio?id="+id);
        return ResponseEntity.ok().contentType(MediaType.parseMediaType("audio/mpeg")).body(audio);
    }
    
    @Autowired
    ManejadorArchivos ma;
    @RequestMapping(value = "/image", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getImage(@RequestParam String id) {
        byte[] image;
        boolean estado = true;
        ma.comprbarExisteFicheroImagen(id);
        try {
            image = ma.descargaImagen(id);
            estado =true;
        } catch (IOException ex) {
            image = new byte[0];
            estado = false;
        }
        logger.log(Level.INFO, "Resultado /image : ["+ estado + "] --- Consulta: /image?id="+id);
        return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(image);
    }
}
