/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.rep;

import com.shimitv.dom.LeccionCompletada;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author helio
 */

@Repository
public interface LeccionCompletadaRep extends CrudRepository<LeccionCompletada, Long> {
    LeccionCompletada findFirstByIdUsuarioOrderByFechaDesc(Long idUsuario); 
    List<LeccionCompletada> findByCurso_IdCursoAndIdUsuario(Long idCurso, Long idUsuario);
    LeccionCompletada findOneByIdUsuarioAndCurso_IdCursoAndIdLeccion(Long idUsuario, Long idCurso, Long idLeccion);
    List<LeccionCompletada> findByCurso_Niveles_IdNivel(Long idNivel);
    List<LeccionCompletada> findByIdUsuarioAndCurso_Niveles_IdNivel(Long idUsuario, Long idNivel);
    LeccionCompletada findFirstByIdUsuarioAndCurso_Niveles_IdNivelOrderByFechaDesc(Long idUsuario, Long idNivel);
    
}