/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.rep;

import com.shimitv.dom.Curso;
import com.shimitv.dom.Leccion;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author helio
 */
@Repository
public interface CursoRep extends CrudRepository<Curso, Long>{
    List<Curso> findByIdiomaOrigen_IdIdioma(Long idIdioma);
}
