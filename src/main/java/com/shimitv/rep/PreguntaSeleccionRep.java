/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.rep;

import com.shimitv.dom.PreguntaSeleccion;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author helio
 */
@Repository
public interface PreguntaSeleccionRep extends CrudRepository<PreguntaSeleccion, Long>{
    List<PreguntaSeleccion> findByIdLeccion(Long idLeccion);
    
}
