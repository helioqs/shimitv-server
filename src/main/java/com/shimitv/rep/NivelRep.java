/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.rep;
 
import com.shimitv.dom.Curso;
import com.shimitv.dom.Nivel;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author helio
 */
@Repository
public interface NivelRep extends CrudRepository<Nivel, Long>{
    List<Nivel> findByCurso_IdCurso(Long idNivel);
}
