/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.rep;

import com.shimitv.dom.Examen;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author helio
 */
@Repository
public interface ExamenRep extends CrudRepository<Examen, Long>{
    List<Examen> findByUsuario_IdUsuarioAndNivel_IdNivel(Long idUsuario, Long idNivel);
}
