/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.rep;

import com.shimitv.dom.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author helio
 */
@Repository
public interface UsuarioRep extends CrudRepository<Usuario, Long>{
    Usuario findOneByNickname(String nickname);
    Usuario findOneByEmail(String email);
    Usuario findOneByCursos_IdCursoAndIdUsuario(Long idCurso, Long idUsuario);
}
