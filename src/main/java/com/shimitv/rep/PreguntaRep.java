/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.rep;

import com.shimitv.dom.Pregunta;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author helio
 */
@Repository
public interface PreguntaRep extends CrudRepository<Pregunta, Long>{
    List<Pregunta> findByIdLeccion(Long idLeccion);
}
