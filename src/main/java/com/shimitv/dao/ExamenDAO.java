/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dao;

import com.shimitv.dom.Examen;
import com.shimitv.dom.Leccion;
import com.shimitv.dom.Nivel;
import com.shimitv.dom.Pregunta;
import com.shimitv.rep.ExamenRep;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author helio
 */
@Repository
public class ExamenDAO {
    @Autowired
    ExamenRep erep;
    @Autowired
    LeccionDAO ldao;
    @Autowired
    PreguntaDAO pdao;
    
    public void saveExamen(Examen e){
        erep.save(e);
    }
    
    public List<Examen> getExamenes(Long idUsuario, Long idNivel){
        return erep.findByUsuario_IdUsuarioAndNivel_IdNivel(idUsuario, idNivel);
    }
    
    public List<Pregunta> crearExamen(Long idUsuario, Nivel nivel) throws SQLException{
        List<Leccion> lecciones = ldao.getLeccionByNivelAndUsuario(idUsuario, nivel);
        List<Pregunta> preguntas = new ArrayList<>();
        for(Leccion l : lecciones){
            List<Pregunta> candidatos = pdao.getCuestionario(l.getIdLeccion());
            preguntas.addAll(candidatos);
        }
        Collections.shuffle(preguntas);
        int i = 0;
        List<Pregunta> preguntasFinal = new ArrayList<>();
        while(i < preguntas.size() && i < 10){
            preguntasFinal.add(preguntas.get(i));
            i++;
        }
        return preguntasFinal;
    }
}
