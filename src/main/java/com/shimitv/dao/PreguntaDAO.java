/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dao;

import com.shimitv.dom.Pregunta;
import com.shimitv.dom.PreguntaEscritura;
import com.shimitv.dom.PreguntaOrdenamiento;
import com.shimitv.dom.PreguntaSeleccion;
import com.shimitv.rep.PreguntaEscrituraRep;
import com.shimitv.rep.PreguntaOrdenamientoRep;
import com.shimitv.rep.PreguntaRep;
import com.shimitv.rep.PreguntaSeleccionRep;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author helio
 */
@Repository
public class PreguntaDAO {
    @Autowired
    private PreguntaRep prep;
    @Autowired
    private PreguntaEscrituraRep perep;
    @Autowired
    private PreguntaSeleccionRep psrep;
    @Autowired
    private PreguntaOrdenamientoRep porep;
    
    private static final Logger logger = Logger.getLogger(PreguntaDAO.class.getName());
    
    public Map<Long, String> preguntaByLeccion(Long idLeccion) throws SQLException{
        Map<Long, String> preguntas = new HashMap<>();
        List<Pregunta> preguntas_leccion = prep.findByIdLeccion(idLeccion);
        for(Pregunta p : preguntas_leccion){
            preguntas.put(p.getIdLeccion(), p.getTipo());
        }
        return preguntas;
    }
    
    public List<PreguntaEscritura> getPreguntaEscritura(Long idLeccion) throws SQLException{        
        return perep.findByIdLeccion(idLeccion);
    }
    
    public List<PreguntaSeleccion> getPreguntaSeleccion(Long idLeccion) throws SQLException{
        return psrep.findByIdLeccion(idLeccion);
    }
    
    public List<PreguntaOrdenamiento> getPreguntaOrdenamiento(Long idLeccion) throws SQLException{
        return porep.findByIdLeccion(idLeccion);
    }
    
    public List<Pregunta> getCuestionario(Long idLeccion) throws SQLException{
        List<Pregunta> preguntas = new ArrayList<>();
        preguntas.addAll(this.getPreguntaSeleccion(idLeccion));
        preguntas.addAll(this.getPreguntaEscritura(idLeccion));
        preguntas.addAll(this.getPreguntaOrdenamiento(idLeccion));
        
        return preguntas;
    }
}
