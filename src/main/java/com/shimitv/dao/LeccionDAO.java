/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dao;
import com.shimitv.dom.Curso;
import com.shimitv.dom.Leccion;
import com.shimitv.dom.LeccionCompletada;
import com.shimitv.dom.Nivel;
import com.shimitv.rep.LeccionCompletadaRep;
import com.shimitv.rep.LeccionRep;
import com.shimitv.rep.NivelRep;
import com.shimitv.rep.UsuarioRep;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Paola
 */
@Repository
public class LeccionDAO {
    @Autowired
    private LeccionRep lrep;
    @Autowired
    private UsuarioRep urep;
    @Autowired
    private LeccionCompletadaRep lcrep;
    @Autowired
    private NivelRep nrep;
    
    private String nameBD="shimidb";
    private static final Logger logger = Logger.getLogger(LeccionDAO.class.getName());
    
    public Leccion getLeccion(Long idLeccion){
        return lrep.findOne(idLeccion);
    }
    //P@ola: Comprobar si existe el la leccion en la bd
    public boolean isLeccion(Long id) throws SQLException {
        Leccion l = lrep.findOne(id);
        if(l == null){
            return false;
        }else{
            return true;
        }
    }
    
    //Devuelve la ultima leccion que haya realizado el proyecto
    public LeccionCompletada getLastLeccion(Long idUsuario, Long idNivel) throws SQLException{
        LeccionCompletada lc = lcrep.findFirstByIdUsuarioAndCurso_Niveles_IdNivelOrderByFechaDesc(idUsuario, idNivel);
        return lc;
    }
    
    public List<LeccionCompletada> getLeccionesCompletadasByNivel(long idNivel) throws SQLException{
        List<LeccionCompletada> lc = lcrep.findByCurso_Niveles_IdNivel(idNivel);
        return lc;
    }
    
    public List<LeccionCompletada> getLeccionesCompletadasByUsuarioAndByNivel(long idUsuario, long idNivel) throws SQLException{
        List<LeccionCompletada> lc = lcrep.findByIdUsuarioAndCurso_Niveles_IdNivel(idUsuario, idNivel);
        return lc;
    }
    
    public List<Leccion> get_lecciones(Long idNivel){
        return lrep.findByNivel_IdNivel(idNivel);
    }
    
    public List<Nivel> get_niveles_curso(Long idCurso){
        return nrep.findByCurso_IdCurso(idCurso);
    }
    
    public List<Leccion> getLeccionByNivelAndUsuario(Long idUsuario, Nivel nivel){
        List<LeccionCompletada> completadas = lcrep.findByCurso_IdCursoAndIdUsuario(nivel.getCurso().getIdCurso() , idUsuario);
        List<Leccion> lecciones = new ArrayList<>();
        for(LeccionCompletada lc : completadas){
            Long idLeccion = lc.getIdLeccion();
            Leccion l = lrep.findOne(idLeccion);
            lecciones.add(l);
        }
        return lecciones;
    }
    
    public List<LeccionCompletada> getLeccionCompletada(Long idUsuario, Long idNivel){
        return lcrep.findByIdUsuarioAndCurso_Niveles_IdNivel(idUsuario, idNivel);
    }
    
     public List<Leccion> getLeccionByNivel(Long idNivel){
        return lrep.findByNivel_IdNivel(idNivel);
    }
    
    
    //P@ola:Registro de Lecciones completadas por usuario
    public boolean registrarLeccionTerminada(Long idLeccion, Long idUser){
        
        if (urep.exists(idUser)){
            Leccion leccion = getLeccion(idLeccion);         
            if ((leccion.getIdLeccion().equals(idLeccion))){
                
                Curso curso= leccion.getCurso();
                LeccionCompletada lc = lcrep.findOneByIdUsuarioAndCurso_IdCursoAndIdLeccion(idUser,curso.getIdCurso(),idLeccion);
                if(lc != null){
                    return false;
                }
                lc = new LeccionCompletada(idUser,idLeccion,curso);
                lcrep.save(lc);
                return true;
            }
        }
        return false;
    }
    
}

