/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dao;

import com.shimitv.dom.Curso;
import com.shimitv.dom.Idioma;
import com.shimitv.dom.Leccion;
import com.shimitv.dom.LeccionCompletada;
import com.shimitv.dom.Usuario;
import com.shimitv.rep.CursoRep;
import com.shimitv.rep.LeccionCompletadaRep;
import com.shimitv.rep.UsuarioRep;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author helio
 */
@Repository
public class UsuarioDAO{
    
    @Autowired
    private UsuarioRep urep;
    @Autowired
    private CursoRep crep;
    @Autowired
    LeccionDAO LEC;
    @Autowired
    private LeccionCompletadaRep lcrep;
    private static final Logger logger = Logger.getLogger(UsuarioDAO.class.getName());

    
    //helio: añadir o actualizar un usuario en la base de datos
    public boolean addUser(String nick,  String mail, String pass){       
        Usuario u = new Usuario(nick,mail,pass);
        urep.save(u);
        return true;
    }
    
    //helio: actualizar un usuario existente en la base de datos
    public boolean updateUsuario(String nick, String nombre, Long Idioma){
        Usuario u = urep.findOneByNickname(nick);
        urep.save(u);
        return true;
    }
    
    public Usuario getUserById(Long idUsuario){
        return urep.findOne(idUsuario);
    }
    
    //helio: extraer un usuario de la base de datos si existe
    public Usuario getUserByNick(String nick){
        return urep.findOneByNickname(nick);
    }
    
    //helio: Permite realizar una búsqueda del usuario usando su correo electrónico
    public Usuario getUserByMail(String email) throws SQLException{
        return urep.findOneByEmail(email);
    }
    
    //helio: verifica si ya está registrado un usuario con el nombre especificado
    public boolean hasUser(String nick){
        Usuario u = urep.findOneByNickname(nick);
        if(u == null){
            return false;
        }else{
            return true;
        }
    }
    
    //helio: Ya existe una cuenta asociada a un correo electrónico
    public boolean hasEmail(String email){
        Usuario u = urep.findOneByEmail(email);
        if(u == null){
            return false;
        }else{
            return true;
        }
    }
    
    //helio: Permite obtener los cursos en los que está matriculado el usuario
    public List<Curso> getCursosByUsuario(String nickname){
        Usuario u = urep.findOneByNickname(nickname);
        return u.getCursos();
    }

    //helio: matricular un usuario en un curso en particular
    public boolean matricular(Long idUsuario, Long idCurso) throws SQLException {
        Usuario u = urep.findOne(idUsuario);
        Curso c = crep.findOne(idCurso);
        u.addCurso(c);
        urep.save(u);
        return true;
    }
    
    //P@ola: Codigo de Logueo de Usuario
    public boolean loguinUser(String nick, String Pass) throws SQLException {
        Usuario existente=new Usuario();
        if (hasEmail(nick)){
            System.out.println("\nLogin Por email\n");
            existente=getUserByMail(nick);
            String pw=existente.getPassword();
            
            if(pw.length()>2){
                if(pw.equals(Pass)){
                    return true;
                }
            }
        }
        else if(hasUser(nick)){
            existente=getUserByNick(nick);
            System.out.println("\nLogin Por Nick\n");
            String pw=existente.getPassword();
            if(pw.length()>2){
                if(pw.equals(Pass)){
                    return true;
                }
            }
        }
        return false;
    }
    
    //P@ola: Comprobar si existe el id de usuario
    public boolean isUser(Long id) throws SQLException {
        Usuario u = urep.findOne(id);
        if (u == null){
            return false;
        }else{
            return true;
        }
    }
    
    //P@ola:Registro de Lecciones completadas por usuario
    public boolean leccionTerminada(Long idLeccion, String idUser){
        
        if (urep.exists(Long.valueOf(idUser))){
            Leccion leccion = LEC.getLeccion(idLeccion);         
            if ((leccion.getIdLeccion().equals(idLeccion))){
                
                Curso curso= leccion.getCurso();
                LeccionCompletada lc = lcrep.findOneByIdUsuarioAndCurso_IdCursoAndIdLeccion(Long.valueOf(idUser),curso.getIdCurso(),idLeccion);
                if(lc != null){
                    return false;
                }
                lc = new LeccionCompletada(Long.valueOf(idUser),idLeccion,curso);
                lcrep.save(lc);
                return true;
            }
        }
        return false;
    }
    
    
    public boolean isMatriculado(Long idUsuario, Long idCurso){
        Usuario u = urep.findOneByCursos_IdCursoAndIdUsuario(idCurso, idUsuario);
        if(u == null){
            return false;
        }else{
            return true;
        }
    }

    
}
