/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shimitv.dao;

import com.shimitv.dom.Curso;
import com.shimitv.dom.Idioma;
import com.shimitv.dom.Nivel;
import com.shimitv.rep.CursoRep;
import com.shimitv.rep.NivelRep;
import com.shimitv.rep.IdiomaRep;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author helio
 */
@Repository
public class CursoDAO {
    @Autowired
    private CursoRep crep;
    @Autowired
    private NivelRep nrep;
    @Autowired
    private IdiomaRep irep;
    
    private static final Logger logger = Logger.getLogger(CursoDAO.class.getName());
     
    //helio: Recuperación de cursos
    public Curso getCurso(Long idCurso){
        return crep.findOne(idCurso);
    }
    
    //helio: Recuperación de niveles
    public Nivel getNivel(Long idNivel){
        return nrep.findOne(idNivel);
    }
    
    //Consulta la existencia de un curso en la base de datos
    public boolean hasCurso(Long idCurso) throws SQLException {
        Curso c = crep.findOne(idCurso);
        if(c == null){
            return false;
        }else{
            return true;
        }
    }  
    
    //Consulta la existencia de un nivel en la base de datos
    public boolean hasNivel(Long idNivel) throws SQLException {
        Nivel n = nrep.findOne(idNivel);
        if(n == null){
            return false;
        }else{
            return true;
        }
    }   
    
    public List<Curso> cursosIdioma(Long idIdioma){
        logger.log(Level.SEVERE, idIdioma.toString());
        return crep.findByIdiomaOrigen_IdIdioma(idIdioma);
    }
    
    public List<Curso> cursos(){
        return (List<Curso>) crep.findAll();
    }
    
    public List<Idioma> idiomas(){
        return (List<Idioma>) irep.findAll();
    }
}