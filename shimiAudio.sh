#!/bin/sh
filename="$1";
output="$2"
counter=0
if [ ! -f $filename ]; then
    echo "No se ha encontrado el archivo"
else
    #Extraer de un archivo en formato csv el texto al cual se le coloca voz
    #Tiene la estructura idioma, texto
    #Genera un texto en el idioma solicitado
    while read line
    do
        idioma=$(echo "$line" | cut -d',' -f1 )
        texto=$(echo "$line" | cut -d',' -f2)
        temp[$counter]="temp"$counter".mp3"
        if [[ "$idioma" == "es" ]]; then
            echo "es"
            espeak-ng -v roa/es-419 -s 150 "$texto" --stdout | ffmpeg -i - -ar 44100 -ac 2 -ab 192k -f mp3 ${temp[$counter]}
        elif [[ "$idioma" == "en" ]]; then
            echo "en"
            espeak-ng -s 150 "$texto" --stdout | ffmpeg -i - -ar 44100 -ac 2 -ab 192k -f mp3 ${temp[$counter]}
        fi
        counter=$(("$counter" + 1)) 
    done < "$filename"
    #Almacenar los nombres de los archivos de audio temporales en una lista
    for i in "${temp[@]}"
    do
        echo "file '$i'" >> lista.txt
    done
    #Usar la lista para concatenar el audio
    ffmpeg -f concat -safe 0 -i lista.txt -c copy -f mp3 "$output"
    #Eliminar los archivos temporales
    rm lista.txt
    for i in "${temp[@]}"
    do
        rm "$i"
    done
fi
